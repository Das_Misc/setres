﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SetRes
{
    internal class Program
    {

        static void Main(string[] args)
        {
            checkArgs(args);

            ScreenSettings.FreeConsole();

            string newResolution;
            if (args[0].ToLower() == "default")
            {
                string defaultResolution = ScreenSettings.GetMaxResolutionWithRefreshRate();
                newResolution = defaultResolution.Split('@')[0];
            }
            else
            {
                newResolution = args[0];
            }

            string[] newRes = newResolution.Split('x');
            int newWidth = int.Parse(newRes[0]);
            int newHeight = int.Parse(newRes[1]);

            if (!ScreenSettings.IsDisplayModeSupported(newWidth, newHeight, out List<string> _))
            {
                Console.WriteLine("Display mode not supported.");
                Environment.Exit(3);
            }

            if (!ScreenSettings.ChangeDisplaySettings(newWidth, newHeight, out string message))
            {
                Console.WriteLine(message);
                Environment.Exit(4);
            }
        }

        private static void checkArgs(string[] args)
        {
            if (args.Length > 1)
            {
                Console.WriteLine("Invalid number of parameters.");
                Environment.Exit(1);
            }

            List<string> supportedModes;
            if (args.Length == 0)
            {
                DeviceMode? mode = ScreenSettings.GetCurrentSettings();
                if (mode != null)
                {
#if DEBUG
                    Debug.WriteLine("Current Mode:\n\t" +
                                    "{0} by {1}, " +
                                    "{2} bit, " +
                                    "{3} degrees, " +
                                    "{4} hertz",
                        mode.Value.dmPelsWidth,
                        mode.Value.dmPelsHeight,
                        mode.Value.dmBitsPerPel,
                        mode.Value.dmDisplayOrientation * 90,
                        mode.Value.dmDisplayFrequency);
#endif

                    Console.WriteLine("Current Mode:\n\t" +
                                      "{0} by {1}, " +
                                      "{2} bit, " +
                                      "{3} degrees, " +
                                      "{4} hertz",
                        mode.Value.dmPelsWidth,
                        mode.Value.dmPelsHeight,
                        mode.Value.dmBitsPerPel,
                        mode.Value.dmDisplayOrientation * 90,
                        mode.Value.dmDisplayFrequency);
                }

                ScreenSettings.IsDisplayModeSupported(0, 0, out supportedModes);
                Console.WriteLine("Supported modes:");
                foreach (string supportedMode in supportedModes)
                    Console.WriteLine(supportedMode);
#if DEBUG
                Debug.WriteLine("Supported modes:");
                foreach (string supportedMode in supportedModes)
                    Debug.WriteLine(supportedMode);
#endif

                Environment.Exit(2);
            }
        }
    }
}
