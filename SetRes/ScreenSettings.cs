﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace SetRes
{
    internal static class ScreenSettings
    {
        #region Constants

        private const int CurrentSettings = -1;
        private const int DisplayChangeSuccessful = 0;     // Indicates that the function succeeded.
        private const int DisplayChangeBadMode = -2;       // The graphics mode is not supported.
        private const int DisplayChangeRestart = 1;        // The computer must be restarted for the graphics mode to work.

        #endregion Constants

        #region DLLImports
        
        [DllImport("kernel32.dll")]
        internal static extern bool FreeConsole();

        [DllImport("User32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumDisplaySettings(
            [param: MarshalAs(UnmanagedType.LPTStr)] string lpszDeviceName,
            [param: MarshalAs(UnmanagedType.U4)] int iModeNum,
            [In, Out] ref DeviceMode lpDevMode);

        [DllImport("User32.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        private static extern int ChangeDisplaySettings(
            [In, Out] ref DeviceMode lpDevMode,
            [param: MarshalAs(UnmanagedType.U4)] uint dwflags);
        
        #endregion DLLImports

        internal static DeviceMode? GetCurrentSettings()
        {
            var mode = new DeviceMode();
            mode.dmSize = (ushort)Marshal.SizeOf(mode);

            if (EnumDisplaySettings(null, CurrentSettings, ref mode)) // Succeeded
                return mode;

            return null;
        }

        internal static string GetMaxResolutionWithRefreshRate()
        {
            IsDisplayModeSupported(0, 0, out List<string> supportedModes);
            return supportedModes[supportedModes.Count - 1];
        }

        internal static bool IsDisplayModeSupported(int width, int height, out List<string> supportedModes)
        {
            var mode = new DeviceMode();
            mode.dmSize = (ushort) Marshal.SizeOf(mode);

            int modeIndex = 0; // 0 = The first mode
            supportedModes = new List<string>();
            string previousSupportedMode = string.Empty;

            while (EnumDisplaySettings(null, modeIndex, ref mode)) // Mode found
            {
                if (mode.dmPelsWidth == (uint)width && mode.dmPelsHeight == (uint)height)
                {
                    supportedModes = supportedModes.Select(res => new { Str = res, Values = res.Split('x') })
                        .OrderBy(x => int.Parse(x.Values[0]))
                        .ThenBy(x => int.Parse(x.Values[1].Split('@')[0]))
                        .ThenBy(x => int.Parse(x.Values[1].Split('@')[1].TrimEnd('h', 'z')))
                        .Select(x => x.Str)
                        .ToList();
                    return true;
                }

                string newSupportedMode = mode.dmPelsWidth + "x" + mode.dmPelsHeight + "@" + mode.dmDisplayFrequency + "hz";
                if (newSupportedMode != previousSupportedMode)
                {
                    supportedModes.Add(newSupportedMode);
                    previousSupportedMode = newSupportedMode;
                }

                modeIndex++; // The next mode
            }

            supportedModes = supportedModes.Select(res => new { Str = res, Values = res.Split('x') })
                .OrderBy(x => int.Parse(x.Values[0]))
                .ThenBy(x => int.Parse(x.Values[1].Split('@')[0]))
                .ThenBy(x => int.Parse(x.Values[1].Split('@')[1].TrimEnd('h', 'z')))
                .Select(x => x.Str)
                .ToList();
            return false;
        }

        internal static bool ChangeDisplaySettings(int width, int height, out string message)
        {
            return ChangeDisplaySettings(width, height, 0, out message);
        }

        internal static bool ChangeDisplaySettings(int width, int height, int refreshRate, out string message)
        {
            var currentMode = new DeviceMode();
            currentMode.dmSize = (ushort) Marshal.SizeOf(currentMode);

            // Retrieving current settings
            // to edit them
            EnumDisplaySettings(null,
                CurrentSettings,
                ref currentMode);

            // Making a copy of the current settings to allow resetting to the original mode
            DeviceMode newMode = currentMode;

            // Changing the settings
            newMode.dmPelsWidth = (uint)width;
            newMode.dmPelsHeight = (uint)height;
            if (refreshRate > 0)
                newMode.dmDisplayFrequency = (uint)refreshRate;

            // Capturing the operation result, 1 = update registry
            int result = ChangeDisplaySettings(ref newMode, 1);

            switch (result)
            {
                case DisplayChangeSuccessful:
                    message = "Success.";
                    return true;

                case DisplayChangeBadMode:
                    message = "Mode not supported.";
                    return false;

                case DisplayChangeRestart:
                    message = "Restart required.";
                    return false;

                default:
                    message = "Failed. Error code = " + result;
                    return false;
            }
        }
    }
}
