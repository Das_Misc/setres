﻿using System.Runtime.InteropServices;

namespace SetRes
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Pointl
    {
        [MarshalAs(UnmanagedType.I4)]
        public int x;
        
        [MarshalAs(UnmanagedType.I4)]
        public int y;
    }
}
